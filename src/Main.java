import java.util.ArrayList;

import it.unisalento.st2.dao.BaseDao;
import it.unisalento.st2.dao.StudentDao;
import it.unisalento.st2.dao.factories.FactoryDao;
import it.unisalento.st2.dao.impl.BaseDaoImpl;
import it.unisalento.st2.dao.impl.StudentDaoImpl;
import it.unisalento.st2.domain.StudentTable;

public class Main {
	
	public static void main(String args[]) {
		
		StudentTable student = new StudentTable();
		
		student.setNome("mannaggia");
		student.setCognome("dio");
		student.setEta(15);
		student.setCitta("LE");
		student.setSesso("M");
		
		StudentDao dao = FactoryDao.getInstance().getStidentDao();
		dao.save(student);
		
	}
	
	
	/*public static void main(String args[]) {
		
		StudentTable student = new StudentTable();
		BaseDao dao = new BaseDaoImpl();
		student = dao.getById(1);
		System.out.println(student.getNome());
		System.out.println(student.getCognome());
	}*/
	
	
	/*public static void main(String args[]) {
	
		StudentTable student = new StudentTable();
		StudentDAO dao = new StudentDAO();
	
		//slist è un oggetto di tipo list ed un oggetto di array-list perche Arraylist è una classe concreta che implementa List
		ArrayList<StudentTable> sList= (ArrayList<StudentTable>) dao.getAll();
		
		//pattern iterator: for su tutti gli elementi che tornano dal db 
		for(StudentTable s: sList){
			System.out.println(s.getNome());
			System.out.println(s.getCognome());
		}
		
		//effettuo l'update del primo nome nel db se la slist non è vuota
		if(!sList.isEmpty()){
			student = sList.get(0);
			student.setNome("umberto");
			dao.update(student); //qui faccio effettivamente l'update
		}
		
		//effettuo il delete
		student=dao.delete(3);
		
		System.out.println(student.getNome());
		
		//riverifico gli oggetti nel db
		for(StudentTable s: dao.getAll()){
			System.out.println(s.getNome());
			System.out.println(s.getCognome());
		}
	}*/
	
	
	/*public static void main(String args[]) {
		StudentDAO dao = new StudentDAO();
		
		for(StudentTable s: dao.getBySurname("campa")){
			System.out.println(s.getNome());
		}
		
	}*/
	
	
	
}
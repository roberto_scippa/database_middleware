package it.unisalento.st2.dao;

import java.util.List;

import it.unisalento.st2.domain.StudentTable;

public interface BaseDao<T> { //la T non è altro che un tipo generico di java(Template in c++)

	/*salviamo nel db*/
	void save(T entity);

	/*getbyid. Aggiungiamo l'oggetto clazz di tipo Class perche gli dovremmo dire a quale classe deve fare riferimento per poter accedere ad una specifica tabella del db*/
	T getById(int id, Class<T> clazz);

	/*select all from db*/
	List<T> getAll(Class<T> clazz);

	/*delete*/
	T delete(int id, Class<T> clazz);

	/*metodo update*/
	void update(T entity) throws Exception; //lancia l'eccezione al metodo/classe che chiama update(al chiamante)

}
package it.unisalento.st2.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	
	private static SessionFactory sessionFactory;
	
	//impostiamo il pattern singelton che sarebbe una classe con metodi statici che non hanno bisogno di essere instanziati.
	//creamo questo codice statico in modo tale che hibernate non istanzi ogni volta un oggetto ma lo fa una volta e al massimo lo aggiorna
	//questo e il modo piu corretto per chreare le sessionfactory
	static { 
		
		Configuration conf = new Configuration();
		conf.configure();//il metodo .configure prende l'informazione dell xml che abbiamo nel progetto e gli importa all interno dell oggetto conf
		
		//serve ad accedere ai servizi esposti da hibernate. Usa il pattern builder
		StandardServiceRegistryBuilder serviceRegBuilder = new StandardServiceRegistryBuilder();
		serviceRegBuilder.applySettings(conf.getProperties());
		ServiceRegistry serviceRegistry =  serviceRegBuilder.build(); //costruendo l'oggetto serviceRegistry con il serviceRegBuilder.build() stiamo dando le configurazioni settate da conf.getproperties()
		sessionFactory = conf.buildSessionFactory(serviceRegistry);

		// A SessionFactory is set up once for an application
	    /*sessionFactory = new Configuration()
	            .configure() // configures settings from hibernate.cfg.xml
	            .buildSessionFactory();
	       */
	}
	

	public static  SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static Session openSession(){
		//ritorna la sessione di hibernate
		return sessionFactory.openSession();
	}

	/*public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}*/
}

package it.unisalento.st2.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import it.unisalento.st2.dao.BaseDao;
import it.unisalento.st2.dao.HibernateUtil;
import it.unisalento.st2.domain.StudentTable;

public class BaseDaoImpl<T> implements BaseDao<T> {
	
	/*salviamo nel db*/
	/* (non-Javadoc)
	 * @see it.unisalento.st2.dao.BaseDao#save(it.unisalento.st2.domain.StudentTable)
	 */
	@Override
	public void save(T entity){
		/*istanzio una sessione hibernate per interagire con il db*/
		Session session = HibernateUtil.openSession(); //HibernateUtil.openSession lancia il blocco static creato nel file hibernateUtil
		session.beginTransaction();
		 //qui effettivamente salvo
		session.save(entity);
		session.getTransaction().commit();
	}
	
	
	/*getbyid*/
	/* (non-Javadoc)
	 * @see it.unisalento.st2.dao.BaseDao#getById(int)
	 */
	@Override
	public T getById(int id, Class<T> clazz){
		/*istanzio una sessione hibernate per interagire con il db*/
		Session session = HibernateUtil.openSession(); //HibernateUtil.openSession lancia il blocco static creato nel file hibernateUtil
		session.beginTransaction();
		/*richiedo un oggetto student:qui faccio effettivamente la get.*/
		T entity = (T) session.get(clazz.getName(), id); //StudentTable.class.getname ritorna il nome della clase e cioe StudentTable
		session.getTransaction().commit();
		session.close();
		
		return entity;
	}
	
	
	/*select all from db*/
	/* (non-Javadoc)
	 * @see it.unisalento.st2.dao.BaseDao#getAll()
	 */
	@Override
	public List<T> getAll(Class<T> clazz){
		/*istanzio una sessione hibernate per interagire con il db*/
		Session session = HibernateUtil.openSession(); //HibernateUtil.openSession lancia il blocco static creato nel file hibernateUtil
		session.beginTransaction();
		
		/*con session.createQuery siamo creando una query*/
		List<T> returnedList = session.createQuery("FROM "+clazz.getName()).list();
		session.getTransaction().commit();
		session.close();
		return 	returnedList;
	}
	
	
	/*delete*/
	/* (non-Javadoc)
	 * @see it.unisalento.st2.dao.BaseDao#delete(int)
	 */
	@Override
	public T delete(int id, Class<T> clazz){
		/*istanzio una sessione hibernate per interagire con il db*/
		Session session = HibernateUtil.openSession(); //HibernateUtil.openSession lancia il blocco static creato nel file hibernateUtil
		session.beginTransaction();
		
		@SuppressWarnings("unchecked")
		T entity = (T) session.get(clazz.getName(), id);
		session.delete(entity);
		session.getTransaction().commit();
		session.close();
		return entity;
	}
	
	
	
	/*metodo update*/
	/* (non-Javadoc)
	 * @see it.unisalento.st2.dao.BaseDao#update(it.unisalento.st2.domain.StudentTable)
	 */
	@Override
	public void update(T entity) throws Exception{ //throw Exception funziona come un try cathc solo che a diff di quello non blocca l'app quando avviene un eccezione
		
			/*istanzio una sessione hibernate per interagire con il db*/
		Session session = HibernateUtil.openSession(); //HibernateUtil.openSession lancia il blocco static creato nel file hibernateUtil
			session.beginTransaction();
		
			session.update(entity);
			session.getTransaction().commit();
			session.close();
		
	}
	

}

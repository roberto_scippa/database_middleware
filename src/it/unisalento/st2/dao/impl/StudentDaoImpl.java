package it.unisalento.st2.dao.impl;

import java.util.List;

import org.hibernate.Session;

import it.unisalento.st2.dao.HibernateUtil;
import it.unisalento.st2.dao.StudentDao;
import it.unisalento.st2.domain.StudentTable;

public class StudentDaoImpl extends BaseDaoImpl<StudentTable> implements StudentDao{
	
	public List<StudentTable> getBySurname(String surname){
		/*istanzio una sessione hibernate per interagire con il db*/
		Session session = HibernateUtil.openSession(); //HibernateUtil.openSession lancia il blocco static creato nel file hibernateUtil
		session.beginTransaction();
		
		/*s è un alias*/
		List<StudentTable> sList = (List<StudentTable>) session.createQuery("FROM "+ StudentTable.class.getName()+ " s where s.cognome=:cognome").setParameter("cognome", surname).list();
		session.getTransaction().commit();
		session.close();
		session.close();
		return sList;
	}
	
	
	
	
}

package it.unisalento.st2.dao;

import java.util.List;

import it.unisalento.st2.domain.StudentTable;

public interface StudentDao extends BaseDao<StudentTable> {
	
	public List<StudentTable> getBySurname(String surname);
	

}

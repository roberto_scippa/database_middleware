package it.unisalento.st2.dao.factories;

import it.unisalento.st2.dao.StudentDao;
import it.unisalento.st2.dao.impl.StudentDaoImpl;

//pattern factory e singelton
public class FactoryDao { //questa classe serve per far si che le diverse implementazioni dei metodi siamo restituiti dalla factory dao e non richiamate singolarmente in ogni classe in modo tale che se cambia l'implkementazione basta cambiare la riga di codice qui dentro e vale per tutto il progetto.

	private static FactoryDao instance;
	
	private StudentDao stidentDao = new StudentDaoImpl();

	public StudentDao getStidentDao() {
		return stidentDao;
	}
	
	//singelton
	public static FactoryDao getInstance(){
		if(instance == null){
			instance = new FactoryDao();
		}
		return instance;
	}
	
}
